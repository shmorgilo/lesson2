package com.artamas.lesson2;

import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;

public final class SomeModule extends AbstractModule {
  @Override
  protected void configure() {
    bindInterceptor(
        Matchers.annotatedWith(MethodCallLogging.class),
        Matchers.any(), new MethodCall()
    );
  }

}
