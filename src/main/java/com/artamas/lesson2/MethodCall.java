package com.artamas.lesson2;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.util.Objects;

public final class MethodCall implements MethodInterceptor {

    @NotNull
    private
    Integer count = 0;


    @Nullable
    @Override
    public Object invoke(@NotNull MethodInvocation invocation) throws Throwable {
        if (Application.type != null) {
            if (Application.type.equals("2")) {
                WriteToConsole();
            } else {
                if (Application.tag != null) {
                    if (Application.type.equals("1")) {
//            this.count++;
//            PrintWriter out = new PrintWriter("filename.txt");
                        WriteToFile();
                    } else {
                        WriteToConsole();
                        WriteToFile();
                    }
                }
            }
        }
        return invocation.proceed();
    }

    private void WriteToConsole() {
        this.count++;
        System.out.println((count%3==0 && Objects.equals(Application.type, "2") ? "3-fold input:" : "")+this.count + "-input");
    }

    private void WriteToFile() throws IOException {
        this.count++;
        FileWriter fw = new FileWriter("out.txt", true);
        fw.write("<" + Application.tag + ">" + this.count + "-input" + "</" + Application.tag + ">\n");
        fw.close();
    }
}
