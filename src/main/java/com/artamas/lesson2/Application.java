package com.artamas.lesson2;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.NoSuchElementException;
import java.util.Scanner;


@SuppressWarnings("FinalOrAbstractClass")
@MethodCallLogging
public class Application {


    @Nullable
    static String type=null;
    @Nullable
    static String tag=null;

    public static void main(@Nullable String[] args) {
        final Injector injector = Guice.createInjector(new SomeModule());
        injector.getInstance(Application.class).waitForInput();
    }

    private void waitForInput() {
        if(type==null){
            System.out.println("Enter type of logging:\n1. File\n2. Console\n3. Composite");
            }
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Waiting for new lines. Key in Ctrl+D to exit.");
            while (true) printToConsole(scanner.next());
        } catch (IllegalStateException | NoSuchElementException ignored) {
        }
    }

    @SuppressWarnings("WeakerAccess")
    public void printToConsole(@NotNull String input){
        if(type==null){
            if(input.equals("1") || input.equals("2") || input.equals("3")){
                type=input;
                System.out.println("You chosen "+(input.equals("1") ? "File" : "")+(input.equals("2") ? "Console" : "")+(input.equals("3") ? "Composite" : "")+" logging");
                if(type.equals("1") || type.equals("3")){
                    System.out.println("Enter tag for logging:");
                }
            }else {
                System.out.println("Enter type of logging:\n1. File\n2. Console\n3. Composite");
            }
        }else{
            if((type.equals("1") || type.equals("3")) && tag==null){
                tag=input;
            }else{
                System.out.println("Just print");
            }
        }
    }
}
